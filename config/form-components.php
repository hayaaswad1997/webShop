<?php

use ProtoneMedia\LaravelFormComponents\Components;
return [
    'prefix' => '',

    /** tailwind | tailwind-2 | bootstrap-4 */
    'framework' => 'tailwind-2',

    'components' => [
        'form' => [
            'view'  => 'form-components::{framework}.form',
            'class' => Components\Form::class,
        ],

        'form-checkbox' => [
            'view'  => 'form-components::{framework}.form-checkbox',
            'class' => Components\FormCheckbox::class,
        ],

        'form-errors' => [
            'view'  => 'form-components::{framework}.form-errors',
            'class' => Components\FormErrors::class,
        ],
    ],
];
