<?php

namespace App\View\Components\partials;

use Illuminate\View\Component;

class price extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $price;

    public function __construct($price = null)
    {
        $this->price = $price;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.partials.price');
    }
}
