<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class LoginPage extends Component
{
    public $email;
    public $password;

    public function loginRules()
    {
        return[
            'email'=>'required|email',
            'password'=>'required'
        ];
    }

    public function loginUser()
    {
        $validatedData = $this->validate($this->loginRules());
//        dd(Auth::user());


        if (Auth::attempt(array('email' => $this->email, 'password' => $this->password))) {
            $this->alert('success', 'Successfully logged in');


//            session()->flash('message', "You are Login successful.");

        } else {
            $this->alert('error', 'email or password are wrong');

//            session()->flash('error', 'email and password are wrong.');

        }
    }

    public function render()
    {
        return view('livewire.login-page')->layout('components.layouts.shop');
    }
}
