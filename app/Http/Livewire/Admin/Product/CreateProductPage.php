<?php

namespace App\Http\Livewire\Admin\Product;

use Livewire\Component;

class CreateProductPage extends Component
{
    public function render()
    {
        return view('livewire.admin.product.create-product-page')->layout('components.layouts.admin');
    }
}
