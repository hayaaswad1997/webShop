<?php

namespace App\Http\Livewire\Admin\Product;

use Livewire\Component;

class ProductHomePage extends Component
{
    public function render()
    {
        return view('livewire.admin.product.product-home-page')->layout('components.layouts.admin');
    }
}
