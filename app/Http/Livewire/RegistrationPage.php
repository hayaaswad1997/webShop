<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class RegistrationPage extends Component
{
    public ?string $first_name = null;
    public ?string $last_name = null;
    public ?string $phone = null;
    public ?string $date_of_birth = null;
    public ?string $email = null;
    public ?string $password = null;

    use LivewireAlert;

    public function registerRules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string',
            'date_of_birth' => 'required|string',
            'email' => 'required|email',
            'password' => 'required',
        ];
    }


    public function registerUser()
    {
        $validatedData = $this->validate($this->registerRules());
        $validatedData['password'] = Hash::make($validatedData['password']);

        if (!$validatedData) {
            return Redirect::back()->withErrors($validatedData);
        }

        return DB::transaction(function () use ($validatedData) {

            $test = User::create($validatedData);
            $this->alert('success', 'Account has been created successfully.');
            sleep(2);
            return redirect()->to('/');
        });

    }


    public function render()
    {
        return view('livewire.registration-page')
            ->layout('components.layouts.shop');
    }
}
