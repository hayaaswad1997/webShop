<?php

use App\Http\Livewire\Admin\AdminHomePage;
use App\Http\Livewire\Admin\Product\CreateProductPage;
use App\Http\Livewire\Admin\Product\ProductHomePage;
use App\Http\Livewire\LoginPage;
use App\Http\Livewire\RegistrationPage;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('livewire.landing-page');
});
Route::get('/admin', AdminHomePage::class);
Route::get('/admin/products', ProductHomePage::class);
Route::get('/admin/product/create', CreateProductPage::class);


Route::get('/register', RegistrationPage::class);
Route::put('/login', LoginPage::class);

