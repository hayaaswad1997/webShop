<div>
    <div>
        <label for="comment" class="block text-sm font-medium text-gray-700">Add your subscription</label>
        <div class="mt-1">
            <textarea rows="4" name="comment" id="comment" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
        </div>
    </div>
</div>
