<div class="py-1">
    @error($field) <span class="flex items-center font-medium tracking-wide text-red-500 text-xs italic">{{ $message }}</span> @enderror
</div>
