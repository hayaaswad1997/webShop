<div class="pl-60  py-8 ">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div>
        <h3 class="text-lg leading-6 font-medium text-gray-900">
           Products
        </h3>

        <!-- This example requires Tailwind CSS v2.0+ -->
        <ul role="list" class="space-y-3 mt-6">
            <a href="/admin/product/create">
                <li class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 shadow overflow-hidden rounded-md px-6 py-4 text-xl">
                    Insert product
                </li>
            </a>

            <a href="/admin/product/view">
                <li class="mt-6 w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 shadow overflow-hidden rounded-md px-6 py-4 text-xl">
                    View products
                </li>
            </a>

            <!-- More items... -->
        </ul>

    </div>
</div>
