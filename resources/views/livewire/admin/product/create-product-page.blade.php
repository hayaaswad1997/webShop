<div class="pl-60  py-8 ">
    <form class="space-y-8 divide-y divide-gray-200">
        <div class="space-y-8 divide-y divide-gray-200">

            <div class="pt-8">
                <div>
                    <h3 class="text-2xl leading-6 font-medium text-gray-900">
                        Create product
                    </h3>
                </div>
                <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                    <div class="sm:col-span-3">
                        <label for="first-name" class="block text-sm font-medium text-gray-700">
                            Product name
                        </label>
                        <div class="mt-1">
                            <input type="text" name="first-name" id="first-name" autocomplete="given-name" class="p-4 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                        </div>
                    </div>

                    <div class="sm:col-span-3">
                        <x-partials.price/>
                    </div>

                    <div class="sm:col-span-3">
                        <label for="email" class="h-8 block text-sm font-medium text-gray-700">
                           Stock
                        </label>
                        <div class="mt-1">
                            <input id="email" name="number" type="number" autocomplete="number" class="p-4 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                        </div>
                    </div>

                    <div class="sm:col-span-3">
                        <label for="country" class="block text-sm font-medium text-gray-700">
                            Category
                        </label>
                        <div class="mt-4">
                            <select id="country" name="country" autocomplete="country-name" class="p-4 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                <option>Flowers</option>
                                <option>Office plants</option>
                                <option>Small trees</option>
                                <option>Seasonal plants</option>
                                <option>Cactuses</option>
                            </select>
                        </div>
                    </div>

                    <div class="sm:col-span-6">
                        <x-partials.text-area/>
                    </div>

                    <div class="sm:col-span-6">
                        <x-partials.upload-image/>
                    </div>
                </div>
            </div>


        </div>

        <div class="pt-5">
            <div class="flex justify-end">
                <button type="button" class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Cancel
                </button>
                <button type="submit" class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Save
                </button>
            </div>
        </div>
    </form>


</div>
