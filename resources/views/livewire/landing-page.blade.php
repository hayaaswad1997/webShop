<x-layouts.shop>

<div>
    <main>
        <!-- Hero section -->
        <div class="relative">
            <!-- Background image and overlap -->
            <div aria-hidden="true" class="hidden absolute inset-0 sm:flex sm:flex-col">
                <div class="flex-1 relative w-full bg-gray-800">
                    <div class="absolute inset-0 overflow-hidden">
                        <img
                            src="https://images.unsplash.com/photo-1602938016996-a03a287ca891?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1742&q=80"
                            alt="" class="w-full h-full object-center object-cover">
                    </div>
                    <div class="absolute inset-0 bg-gray-900 opacity-50"></div>
                </div>
                <div class="w-full bg-white h-32 md:h-40 lg:h-48"></div>
            </div>

            <section aria-labelledby="category-heading" class="pt-24 sm:pt-32 xl:max-w-7xl xl:mx-auto xl:px-8">
                <div class="px-4 sm:px-6 sm:flex sm:items-center sm:justify-between lg:px-8 xl:px-0">
                    <h2 id="category-heading" class="text-2xl font-extrabold tracking-tight text-gray-900">Shop by
                        Category</h2>
                    <a href="#" class="hidden text-sm font-semibold text-indigo-600 hover:text-indigo-500 sm:block">Browse
                        all categories<span aria-hidden="true"> &rarr;</span></a>
                </div>

                <div class="mt-4 flow-root">
                    <div class="-my-2">
                        <div class="box-content py-2 relative h-80 overflow-x-auto xl:overflow-visible">
                            <div
                                class="absolute min-w-screen-xl px-4 flex space-x-8 sm:px-6 lg:px-8 xl:relative xl:px-0 xl:space-x-0 xl:grid xl:grid-cols-5 xl:gap-x-8">
                                <a href="#"
                                   class="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto">
                <span aria-hidden="true" class="absolute inset-0">
                  <img src="https://images.unsplash.com/photo-1617111490936-07b47eafdcd4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=752&q=80" alt=""
                       class="w-full h-full object-center object-cover">
                </span>
                                    <span aria-hidden="true"
                                          class="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"></span>
                                    <span
                                        class="relative mt-auto text-center text-xl font-bold text-white">Flowers</span>
                                </a>

                                <a href="#"
                                   class="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto">
                <span aria-hidden="true" class="absolute inset-0">
                  <img src="https://images.unsplash.com/photo-1554936198-b66e9f8ec01a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" alt=""
                       class="w-full h-full object-center object-cover">
                </span>
                                    <span aria-hidden="true"
                                          class="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"></span>
                                    <span
                                        class="relative mt-auto text-center text-xl font-bold text-white">Office plants</span>
                                </a>

                                <a href="#"
                                   class="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto">
                <span aria-hidden="true" class="absolute inset-0">
                  <img src="https://images.unsplash.com/photo-1575574202425-ba42a224118b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" alt=""
                       class="w-full h-full object-center object-cover">
                </span>
                                    <span aria-hidden="true"
                                          class="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"></span>
                                    <span
                                        class="relative mt-auto text-center text-xl font-bold text-white">small trees</span>
                                </a>

                                <a href="#"
                                   class="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto">
                <span aria-hidden="true" class="absolute inset-0">
                  <img src="https://images.unsplash.com/photo-1490772888775-55fceea286b8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80" alt=""
                       class="w-full h-full object-center object-cover">
                </span>
                                    <span aria-hidden="true"
                                          class="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"></span>
                                    <span
                                        class="relative mt-auto text-center text-xl font-bold text-white">Seasonal plants</span>
                                </a>

                                <a href="#"
                                   class="relative w-56 h-80 rounded-lg p-6 flex flex-col overflow-hidden hover:opacity-75 xl:w-auto">
                <span aria-hidden="true" class="absolute inset-0">
                  <img src="https://images.unsplash.com/photo-1509587584298-0f3b3a3a1797?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=826&q=80" alt=""
                       class="w-full h-full object-center object-cover">
                </span>
                                    <span aria-hidden="true"
                                          class="absolute inset-x-0 bottom-0 h-2/3 bg-gradient-to-t from-gray-800 opacity-50"></span>
                                    <span class="relative mt-auto text-center text-xl font-bold text-white">cactuses</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 px-4 sm:hidden">
                    <a href="#" class="block text-sm font-semibold text-indigo-600 hover:text-indigo-500">Browse all
                        categories<span aria-hidden="true"> &rarr;</span></a>
                </div>
            </section>


        </div>

        <section aria-labelledby="trending-heading">
            <div class="max-w-7xl mx-auto py-24 px-4 sm:px-6 sm:py-32 lg:pt-32 lg:px-8">
                <div class="md:flex md:items-center md:justify-between">
                    <h2 id="favorites-heading" class="text-2xl font-extrabold tracking-tight text-gray-900">Most wanted</h2>
                    <a href="#" class="hidden text-sm font-medium text-indigo-600 hover:text-indigo-500 md:block">See more<span aria-hidden="true"> &rarr;</span></a>
                </div>

                <div class="mt-6 grid grid-cols-2 gap-x-4 gap-y-10 sm:gap-x-6 md:grid-cols-4 md:gap-y-0 lg:gap-x-8">
                    <div class="group relative">
                        <div class="w-full h-56 rounded-md overflow-hidden group-hover:opacity-75 lg:h-72 xl:h-80">
                            <img src="https://images.unsplash.com/photo-1602605591678-2ae3e39ebb0d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"
                                 alt="Hand stitched, orange leather long wallet."
                                 class="w-full h-full object-center object-cover">
                        </div>
                        <h3 class="mt-4 text-sm text-gray-700">
                            <a href="#">
                                <span class="absolute inset-0"></span>
                                Iris
                            </a>
                        </h3>
                        <p class="mt-1 text-sm text-gray-500">outside plant</p>
                        <p class="mt-1 text-sm font-medium text-gray-900">€ 15.00</p>
                    </div>

                    <div class="group relative">
                        <div class="w-full h-56 rounded-md overflow-hidden group-hover:opacity-75 lg:h-72 xl:h-80">
                            <img src="https://images.unsplash.com/photo-1524598171353-ce84a157ed05?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80"
                                 alt="Hand stitched, orange leather long wallet."
                                 class="w-full h-full object-center object-cover">
                        </div>
                        <h3 class="mt-4 text-sm text-gray-700">
                            <a href="#">
                                <span class="absolute inset-0"></span>
                                Orchid
                            </a>
                        </h3>
                        <p class="mt-1 text-sm text-gray-500">Inside plant</p>
                        <p class="mt-1 text-sm font-medium text-gray-900">€ 17.99</p>
                    </div>
                </div>

                <div class="mt-8 text-sm md:hidden">
                    <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">Shop the collection<span
                            aria-hidden="true"> &rarr;</span></a>
                </div>
            </div>
        </section>

        <section aria-labelledby="perks-heading" class="bg-gray-50 border-t border-gray-200">
            <h2 id="perks-heading" class="sr-only">Our perks</h2>

            <div class="max-w-7xl mx-auto py-24 px-4 sm:px-6 sm:py-32 lg:px-8">
                <div class="grid grid-cols-1 gap-y-12 sm:grid-cols-2 sm:gap-x-6 lg:grid-cols-4 lg:gap-x-8 lg:gap-y-0">
                    <div class="text-center md:flex md:items-start md:text-left lg:block lg:text-center">
                        <div class="md:flex-shrink-0">
                            <div class="flow-root">
                                <img class="-my-1 h-24 w-auto mx-auto"
                                     src="https://tailwindui.com/img/ecommerce/icons/icon-returns-light.svg" alt="">
                            </div>
                        </div>
                        <div class="mt-6 md:mt-0 md:ml-4 lg:mt-6 lg:ml-0">
                            <h3 class="text-sm font-semibold tracking-wide uppercase text-gray-900">
                                Free returns
                            </h3>
                            <p class="mt-3 text-sm text-gray-500">
                                Not what you expected? Place it back in the parcel and attach the pre-paid postage
                                stamp.
                            </p>
                        </div>
                    </div>

                    <div class="text-center md:flex md:items-start md:text-left lg:block lg:text-center">
                        <div class="md:flex-shrink-0">
                            <div class="flow-root">
                                <img class="-my-1 h-24 w-auto mx-auto"
                                     src="https://tailwindui.com/img/ecommerce/icons/icon-calendar-light.svg" alt="">
                            </div>
                        </div>
                        <div class="mt-6 md:mt-0 md:ml-4 lg:mt-6 lg:ml-0">
                            <h3 class="text-sm font-semibold tracking-wide uppercase text-gray-900">
                                Same day delivery
                            </h3>
                            <p class="mt-3 text-sm text-gray-500">
                                We offer a delivery service that has never been done before. Checkout today and receive
                                your products within hours.
                            </p>
                        </div>
                    </div>

                    <div class="text-center md:flex md:items-start md:text-left lg:block lg:text-center">
                        <div class="md:flex-shrink-0">
                            <div class="flow-root">
                                <img class="-my-1 h-24 w-auto mx-auto"
                                     src="https://tailwindui.com/img/ecommerce/icons/icon-gift-card-light.svg" alt="">
                            </div>
                        </div>
                        <div class="mt-6 md:mt-0 md:ml-4 lg:mt-6 lg:ml-0">
                            <h3 class="text-sm font-semibold tracking-wide uppercase text-gray-900">
                                All year discount
                            </h3>
                            <p class="mt-3 text-sm text-gray-500">
                                Looking for a deal? You can use the code &quot;ALLYEAR&quot; at checkout and get money
                                off all year round.
                            </p>
                        </div>
                    </div>

                    <div class="text-center md:flex md:items-start md:text-left lg:block lg:text-center">
                        <div class="md:flex-shrink-0">
                            <div class="flow-root">
                                <img class="-my-1 h-24 w-auto mx-auto"
                                     src="https://tailwindui.com/img/ecommerce/icons/icon-planet-light.svg" alt="">
                            </div>
                        </div>
                        <div class="mt-6 md:mt-0 md:ml-4 lg:mt-6 lg:ml-0">
                            <h3 class="text-sm font-semibold tracking-wide uppercase text-gray-900">
                                For the planet
                            </h3>
                            <p class="mt-3 text-sm text-gray-500">
                                We’ve pledged 1% of sales to the preservation and restoration of the natural
                                environment.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
</x-layouts.shop>
